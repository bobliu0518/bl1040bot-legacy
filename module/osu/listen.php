﻿<?php

global $Queue, $Event;
use PhpZip\ZipFile;
loadModule('credit.tools');

$Queue[]= sendBack('Processing…');

$webHeader = [
    "http" => [
        "header" => 'Cookie: '.config('osu_cookie')
    ]
];

$beatmapSetID = (int)nextArg();

$temp = getData("osu/listen/{$beatmapSetID}.mp3");
if($temp !== false){
    decCredit($Event['user_id'], 100);
    $Queue[]= sendBack(sendRec($temp));
    $Queue[]= sendBack('点歌成功，收费100金币，你现在的余额为 '.getCredit($Event['user_id']));
    leave();
}
decCredit($Event['user_id'], 500); 

$osz = new ZipFile();
$web = file_get_contents('https://osu.ppy.sh/beatmapsets/'.$beatmapSetID.'/download', false, stream_context_create($webHeader));
if(!$web){addCredit($Event['user_id'], 500);leave('获取歌曲失败！');}

try{
    $osz->openFromString($web);
}catch(\Exception $e){addCredit($Event['user_id'], 500);leave('无法打开谱面，请联系2018962389！');}

$oszFiles = $osz->matcher();

$mp3FileName = $oszFiles->match('~\S*\.mp3~')->getMatches()[0];

$mp3 = $osz->getEntryContents($mp3FileName);
setData("osu/listen/{$beatmapSetID}.mp3", $mp3);

$Queue[]= sendBack(sendRec($mp3));
$Queue[]= sendBack('点歌成功，收费500金币，你现在的余额为 '.getCredit($Event['user_id']));

?>