﻿<?php

global $Queue;

$msg=<<<EOT
让度娘说话
用法：
#voice [声音代号] {语言代号}
{
    文本
}

可用声音代号：
0 死板女声
1 死板男声
3 矫情男声
4 矫情女声
不指定声音代号时默认0。

可用语言代号：
zh 汉语
EOT;

$Queue[]= sendBack($msg);

?>