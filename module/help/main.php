﻿<?php

global $Queue;
$msg=<<<EOT
说明：
在 help 信息中，大括号表示必须项，中括号表示可选项，
管线符表示左右均可以，（发送命令时请注意不要加括号，
并且使用半角字符）如果你的参数包含空格，可以使用英
单双引号。发送
#help.{命令}[.下一级命令]
获得特定命令的帮助
普通用户可用命令：
checkin credit issue osu pixiv recordStat roll
search sleep time trans unsleep version voice
管理员用户(待定)
超级管理员可用命令为普通用户可用命令，以及：
AD announce
EOT;

$Queue[]= sendBack($msg);

?>
