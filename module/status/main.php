<?php

global $Queue;
use Linfo\Linfo;
requireSeniorAdmin();

$linfo = new Linfo();
$parser = $linfo->getParser();

$load = $parser->getLoad();
$ram = $parser->getRam();
$uptime = $parser->getUpTime();
$disk0 = $parser->getMounts()[0];
$disk1 = $parser->getMounts()[1];
$disk2 = $parser->getMounts()[2];
$disk3 = $parser->getMounts()[3];

$usedRam = sprintf('%.2fG', ($ram['total']-$ram['free'])/1000/1000/1000);
$totalRam = sprintf('%.2fG', $ram['total']/1000/1000/1000);
$usedRamPercent = sprintf('%.2f%%', ($ram['total']-$ram['free'])/$ram['total']*100);

$usedDisk0 = sprintf('%.2fG', $disk0['used']/1000/1000/1000);
$totalDisk0 = sprintf('%.2fG', $disk0['size']/1000/1000/1000);
$usedDiskPercent0 = sprintf('%.2f%%', $disk0['used']/$disk0['size']*100);

$usedDisk1 = sprintf('%.2fG', $disk1['used']/1000/1000/1000);
$totalDisk1 = sprintf('%.2fG', $disk1['size']/1000/1000/1000);
$usedDiskPercent1 = sprintf('%.2f%%', $disk1['used']/$disk1['size']*100);

$usedDisk2 = sprintf('%.2fG', $disk2['used']/1000/1000/1000);
$totalDisk2 = sprintf('%.2fG', $disk2['size']/1000/1000/1000);
$usedDiskPercent2 = sprintf('%.2f%%', $disk2['used']/$disk2['size']*100);

$usedDisk3 = sprintf('%.2fG', $disk3['used']/1000/1000/1000);
$totalDisk3 = sprintf('%.2fG', $disk3['size']/1000/1000/1000);
$usedDiskPercent3 = sprintf('%.2f%%', $disk3['used']/$disk3['size']*100);

$msg=<<<EOT
[System]
Load: {$load['now']} {$load['5min']} {$load['15min']}
Mem:  {$usedRam}/{$totalRam} ({$usedRamPercent})
Disk0: {$usedDisk0}/{$totalDisk0} ({$usedDiskPercent0})
Disk1: {$usedDisk1}/{$totalDisk1} ({$usedDiskPercent1})
Disk2: {$usedDisk2}/{$totalDisk2} ({$usedDiskPercent2})
ExtDisk: {$usedDisk3}/{$totalDisk3} ({$usedDiskPercent3})
Up:   {$uptime['text']}
EOT;

$opcache = opcache_get_status(false);
if(is_array($opcache)){
    $opcStatus = $opcache['opcache_statistics'];
    $opcMemWasteRate = sprintf('%.2f%%', $opcache['memory_usage']['current_wasted_percentage']*100);
    $opcHitRate = sprintf('%.2f%%', $opcStatus['opcache_hit_rate']);
    $msg.=<<<EOT


[OPcache]
Mem Waste Rate: {$opcMemWasteRate}
Cached/Max: ({$opcStatus['num_cached_scripts']}){$opcStatus['num_cached_keys']}/{$opcStatus['max_cached_keys']}
Hits/Miss: {$opcStatus['hits']}/{$opcStatus['misses']} ({$opcHitRate})
Restart(OOM Hash): {$opcStatus['oom_restarts']} {$opcStatus['hash_restarts']}
EOT;
}

$Queue[]= sendBack($msg);

?>
